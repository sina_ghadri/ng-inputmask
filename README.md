###  can masking the value by this library.

### Usage
```html
<input class="form-control" mask="yyyy/MM/dd" mask-placeholder="____/__/__" dir="ltr"/>
```
### Mask
```
Number: 9
Character: A
Year: yyyy
Month: MM
Day: dd
Hour: hh
Minute: mm
Second: ss
Skip Pattern: \A = A
Custom Regex : ['^[0-5]$','^[0-9]$'] => 00 ~ 59
```
