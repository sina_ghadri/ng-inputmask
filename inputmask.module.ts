import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { InputMaskDirective } from './inputmask.directive';

@NgModule({
  imports: [FormsModule,BrowserModule,],
  declarations: [InputMaskDirective],
  exports: [InputMaskDirective]
})
export class InputMaskModule { }
